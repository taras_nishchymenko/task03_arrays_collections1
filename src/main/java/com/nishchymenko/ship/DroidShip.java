package com.nishchymenko.ship;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class DroidShip<K, D extends Droid> {

  private Map<K, D> droids = new HashMap<>();

  public D getDroid(K key) {
    return droids.get(key);
  }

  public void put(K key, D droid) {
    droids.put(key, droid);
  }

  public void remove(K key) {
    droids.remove(key);
  }

  public void putAll(Map<? extends K, ? extends D> collection) {
    droids.putAll(collection);
  }

  public Map<K, D> getAll(Collection<? extends K> keys) {
    Map<K, D> result = new HashMap<>();

    for (Object key : keys) {
      D droid = droids.get(key);
      if (droid != null) {
        result.put((K)key, droid);
      }
    }

    return result;
  }

  public int size() {
    return droids.size();
  }
}
