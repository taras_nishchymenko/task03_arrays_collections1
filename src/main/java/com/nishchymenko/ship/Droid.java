package com.nishchymenko.ship;

import java.util.Objects;

public class Droid {

  private int power;

  public Droid(int power) {
    this.power = power;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Droid droid = (Droid) o;
    return power == droid.power;
  }

  @Override
  public int hashCode() {
    return Objects.hash(power);
  }
}
