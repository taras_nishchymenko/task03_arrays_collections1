package com.nishchymenko.logical;

public class Tasks {

  public int[] deleteMoreThenTwo(int[] array) {
    int[] result = new int[array.length];
    int index = 0;

    for (int i = 1; i < array.length; i++) {
      if (!containsMoreThenTwoTimes(array, array[i])) {
        if (!contains(result, array[i])) {
          result[index] = array[i];
          index++;
        }
      }
    }
    result = copy(result, index);

    return result;
  }

  public int[] deleteSeries(int[] array) {
    int[] result = new int[array.length];
    result[0] = array[0];
    int prev = array[0];
    int index = 1;

    for (int i = 1; i < array.length; i++) {
      if (array[i] != prev) {
        result[index] = array[i];
        index++;
      }
      prev = array[i];
    }
    result = copy(result, index);

    return result;
  }

  public int[] elementsInOneArray(int[] first, int[] second) {
    int[] result = new int[first.length];
    int index = 0;

    for (int i = 0; i < result.length; i++) {
      if (contains(result, first[i])) {
        break;
      }
      if (!contains(second, first[i])) {
        result[index] = first[i];
        index++;
      }
    }
    result = copy(result, index);

    return result;
  }

  public int[] elementsInBothArrays(int[] first, int[] second) {
    int size = first.length < second.length ? first.length : second.length;
    int[] result = new int[size];
    int index = 0;

    for (int i = 0; i < first.length; i++) {
      if (contains(result, first[i])) {
        break;
      }
      for (int j = 0; j < second.length; j++) {
        if (first[i] == second[j]) {
          result[index] = first[i];
          index++;
          break;
        }
      }
    }

    result = copy(result, index);

    return result;
  }

  private int[] copy(int[] input, int count) {
    int[] result = new int[count];
    for (int i = 0; i < count; i++) {
      result[i] = input[i];
    }
    return result;
  }

  private boolean contains(int[] result, int number) {
    for (int i : result) {
      if (i == number) {
        return true;
      }
    }
    return false;
  }

  private boolean containsMoreThenTwoTimes(int[] result, int number) {
    int count = 0;
    for (int i : result) {
      if (i == number) {
        count++;
      }
    }
    return count > 2;
  }

}
