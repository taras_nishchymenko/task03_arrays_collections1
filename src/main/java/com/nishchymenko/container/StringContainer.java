package com.nishchymenko.container;

public class StringContainer {

  private String[] entry;
  private int size;

  public StringContainer() {
    entry = new String[10];
    size = 0;
  }

  public void add(String string) {
    if (size > entry.length - 1) {
      resize();
    }
    entry[size] = string;
    size++;
  }

  private void resize() {
    String[] newEntry = new String[size * 2];
    for (int i = 0; i < size; i++) {
      newEntry[i] = entry[i];
    }
    entry = newEntry;
  }

  public String get(int index) {
    return entry[index];
  }

  public int size() {
    return size;
  }

}
