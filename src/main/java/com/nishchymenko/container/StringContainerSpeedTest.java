package com.nishchymenko.container;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class StringContainerSpeedTest {

  public static void main(String[] args) {
    List<String> list = new ArrayList<>();
    StringContainer container = new StringContainer();
    int iterations = 10_000_000;

    long time = new Date().getTime();
    for (int i = 0; i < iterations; i++) {
      list.add("string" + 1);
    }
    time = new Date().getTime() - time ;
    System.out.println("ArrayList time " + time);

    time = new Date().getTime();
    for (int i = 0; i < iterations; i++) {
      container.add("string" + 1);
    }
    time = new Date().getTime() - time ;
    System.out.println("StringContainer time " + time);
  }

}
