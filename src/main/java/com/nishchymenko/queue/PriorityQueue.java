package com.nishchymenko.queue;

public class PriorityQueue<T extends Comparable> {

  private class Node {
    T entry;
    Node next;
    Node prev;

    public Node(T entry, Node next, Node prev) {
      this.entry = entry;
      this.next = next;
      this.prev = prev;
    }
  }

  private Node head;

  public T peek() {
    return head.entry;
  }

  public T pool() {
    T result = head.entry;
    head = head.next;
    head.prev = null;

    return result;
  }

  public void add(T t) {
    add(t, head);
  }

  private void add(T t, Node head) {

    if (head == null) {
      this.head = new Node(t, null, null);
      return;
    }

    if (head.next == null) {

      if (t.compareTo(head.entry) >= 0) {
        Node node = new Node(t, head, head.prev);
        if (head.prev != null) {
          head.prev.next = node;
        } else {
          this.head = node;
        }
        head.prev = node;
        return;
      }
      head.next = new Node(t, null, head);
      return;
    }

    int result = t.compareTo(head.entry);

    if (result >= 0) {
      Node node = new Node(t, head, head.prev);
      if (head.prev != null) {
        head.prev.next = node;
      } else {
        this.head = node;
      }
      head.prev = node;
    } else {
      add(t, head.next);
    }
  }
}
