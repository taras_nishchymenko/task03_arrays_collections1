package com.nishchymenko.logical;

import static org.junit.Assert.*;

import org.junit.Test;

public class TasksTest {

  @Test
  public void elementsInBothArraysTest() {
    Tasks tasks = new Tasks();
    int[] first = {1, 3, 2, 4, 3, 5};
    int[] second = {1, 3, 4, 1, 0, 4};

    int[] result = {1, 3, 4};
    assertArrayEquals(result, tasks.elementsInBothArrays(first,second));
  }

  @Test
  public void elementsInOneArrayTest() {
    Tasks tasks = new Tasks();
    int[] first = {1, 3, 2, 4, 3, 5};
    int[] second = {1, 3, 4, 1, 0, 4};

    int[] result = {2, 5};
    assertArrayEquals(result, tasks.elementsInOneArray(first,second));
  }

  @Test
  public void deleteSeriesTest() {
    Tasks tasks = new Tasks();
    int[] array = {1, 1, 1, 2, 2, 1, 1, 3, 4, 4};
    int[] result = {1, 2, 1, 3, 4};
    assertArrayEquals(result, tasks.deleteSeries(array));
  }

  @Test
  public void deleteMoreThenTwoTimesTest() {
    Tasks tasks = new Tasks();
    int[] array = {1, 1, 1, 2, 2, 1, 1, 3, 4, 4};
    int[] result = {2, 3, 4};
    assertArrayEquals(result, tasks.deleteMoreThenTwo(array));
  }
}