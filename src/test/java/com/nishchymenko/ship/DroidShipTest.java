package com.nishchymenko.ship;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;

public class DroidShipTest {

  private DroidShip<Number, Droid> ship = new DroidShip<>();

  @Before
  public void before() {
    ship.put(1, new R2_D2());
    ship.put(1., new C_3PO());
    ship.put(2, new BB_8());
    ship.put(2., new R2_D2());
    ship.put(3, new C_3PO());
    ship.put(3., new BB_8());
  }

  @Test
  public void sizeTest() {
    assertEquals(6, ship.size());
    ship.remove(1);
    assertEquals(5, ship.size());
  }

  @Test
  public void getTest() {
    assertEquals(new R2_D2(), ship.getDroid(2.));
  }

  @Test
  public void putAllTest() {
    Map<Integer, R2_D2> r2_d2Map = new HashMap<>();
    r2_d2Map.put(4, new R2_D2());
    r2_d2Map.put(5, new R2_D2());
    ship.putAll(r2_d2Map);
    assertEquals(8, ship.size());
  }

  @Test
  public void getAllTest() {
    List<Double> keys = Arrays.asList(1., 2., 3.);
    Map<Number, Droid> result = ship.getAll(keys);
    assertEquals(3, result.size());
  }

}