package com.nishchymenko.container;

import static org.junit.Assert.*;

import org.junit.Test;

public class StringContainerTest {

  @Test
  public void containerTest() {
    StringContainer container = new StringContainer();

    for (int i = 0; i < 20; i++) {
      container.add("");
    }

    assertEquals(20, container.size());
  }

}